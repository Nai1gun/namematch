'''
Created on Nov 20, 2012

@author: nailgun
'''
import re
from main import regex_builder, splitter

class Comparator(object):

    def __init__(self):
        '''
        Constructor
        '''
        pass
        
    def compare(self, source):
        match1 = Comparator.__compare_lists(source.first_list, source.second_list)
        match2 = Comparator.__compare_lists(source.second_list, source.first_list)
        if match1 > match2:
            match = match1
        else:
            match = match2
        return match
    
    @staticmethod
    def __compare_lists(source1, source2):
        match = -1
        for item1 in source1:
            for item2 in source2:
                subMatch = Comparator.__compare_items(item1, item2)
                if subMatch > match:
                    match = subMatch
        return match
    
    @staticmethod
    def __compare_items(item1, item2):
        if item1 == None or item2 == None:
            return -1
        regex = regex_builder.create_regex(item1)
        match = re.search(regex, item2)
        if match != None:
            return 1
        if Comparator.maybe_match(item1, item2):
            return 0
        return -1

    @staticmethod
    def maybe_match(item1, item2):
        for word1 in splitter.split(item1):
            for word2 in splitter.split(item2):
                if (word1 == word2):
                    return True
        return False