'''
Created on Nov 20, 2012

@author: nailgun
'''
import re
from main.special_words import ORGANIZATION_FORMS
def create_regex(name):
    regex =  '^' + re.escape(name) + '$'
    regex = regex.replace('\.', '[\.\ ]?')
    regex = regex.replace('\,', '[\,\ ]?')
    regex = regex.replace('\ AND\ ', '\ \&\ ')
    regex = regex.replace('\ \&\ ', '\ (\&|AND)\ ')
    regex = regex.replace('\ \(THE\)', '(\ \(THE\))?')
    org_regex = "(\ %s)?" % '|\ '.join(ORGANIZATION_FORMS)
    for org in ORGANIZATION_FORMS:
        org = re.escape(org)
        org = '\ %s' % org
        if regex.count(org) == 1:
            regex = regex.replace(org, org_regex)
            break
    return regex