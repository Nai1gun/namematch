from main.special_words import ORGANIZATION_FORMS, NOT_IMPORTANT

def split(str):
    str = str.replace(",", "")
    str = str.replace(".", "")
    for org in ORGANIZATION_FORMS:
        str = str.replace(" " + org, "")
    for not_important in NOT_IMPORTANT:
        str = str.replace(" " + not_important, "")
    return str.split(" ")