'''
Created on Nov 20, 2012

@author: nailgun
'''

from main.excel_processor import ExcelProcessorOpenpyxlImpl
from main.comparator import Comparator
from main.excel_writer import ExcelWriter

COMPARATOR = Comparator()

if __name__ == '__main__':
#    print(create_regex('hey!'))
#    s = Source(["A & B,C,D", "gagaga"], ["A AND BCD", "mimimi"])
#    print(s)
#    c = Comparator()
#    print(c.compare(s))
#    pass
    processor = ExcelProcessorOpenpyxlImpl(
           '../NameMatch_Part2_LE.xlsx')

    writer = ExcelWriter('../NameMatch_Part2_LE_edit.xlsx')

    while True:
        s = processor.get_next_source()
        if s == None:
            break
        else:
            row = processor.current_row
            print 'first: %s' % s.first_list
            print 'second: %s' % s.second_list
            result = COMPARATOR.compare(s)
            print(result)
            writer.write_new_line(row, result)

    writer.save()