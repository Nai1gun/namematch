from openpyxl import Workbook
wb = Workbook(optimized_write = True)

ws = wb.create_sheet()

# now we'll fill it with 10 rows x 5 columns
for irow in xrange(10):
    ws.append(['%d' % i for i in xrange(5)])

wb.save('../new_big_file.xlsx')