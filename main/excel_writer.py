from openpyxl import Workbook

class ExcelWriter(object):

    def __init__(self, file_path):
        self.wb = Workbook(optimized_write = True)
        self.ws = self.wb.create_sheet()
        self.file_path = file_path

    def write_new_line(self, row, append_num):
        list = []
        for cell in row:
            list.append(cell.internal_value)
        list.append(append_num)
        self.ws.append(list)

    def save(self):
        self.wb.save(self.file_path)