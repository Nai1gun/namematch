'''
Created on Nov 20, 2012

@author: nailgun
'''

class Source(object):
    '''
    This class contains data to compare. 
    Two sources, several versions for each.
    '''


    def __init__(self, first_list, second_list):
        '''
        Constructor
        '''
        self.first_list = first_list
        self.second_list = second_list
        
    def __repr__(self):
        return "Source 1: %s ; source 2: %s" % (self.first_list, self.second_list)
    
    