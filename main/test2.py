from main.domain import Source

from main.excel_processor import ExcelProcessorOpenpyxlImpl
from main.comparator import Comparator
from main.regex_builder import create_regex

COMPARATOR = Comparator()

if __name__ == '__main__':
    s = Source(["K. DHANDAPANI & CO.LTD."], ["K DHANDAPANI & CO"])
    print(s)
    c = Comparator()
    print(c.compare(s))