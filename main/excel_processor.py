'''
Created on Nov 25, 2012

@author: nailgun
'''
#from reader.excel import load_workbook
from openpyxl import load_workbook
from main.domain import Source

SHEET_NAME = 'NameMatch'

FIRST_LIST = 'first_list'

SECOND_LIST = 'second_list'

COLUMNS = {
   FIRST_LIST: ('OSIRIS name', 'OSIRIS Previous prv_name'),
   SECOND_LIST: ('Datastream ECNAME', 'Previous Name - PNAME')}

class ExcelProcessorOpenpyxlImpl(object):
    
    first_list_columns = []
    
    second_list_columns = []

    current_row = None

    def __init__(self, file_path):        

        wb = load_workbook(filename = file_path
                       , use_iterators=True)

        ws = wb.get_sheet_by_name(name = SHEET_NAME)
    
#        for row in ws.iter_rows(): # it brings a new method: iter_rows()

        self.iterator = ws.iter_rows()
        
        column = 0

        for cell in self.iterator.next():

            cell = cell.internal_value
            
            if cell in COLUMNS[FIRST_LIST]:
                self.first_list_columns.append(column)
            else:
                if cell in COLUMNS[SECOND_LIST]:
                    self.second_list_columns.append(column)
            
            column += 1
            
        print self.first_list_columns
        print self.second_list_columns
        
    def get_next_source(self):
        
        first_list = []
        
        second_list = []
        
        column = 0
        
        try:

            self.current_row = self.iterator.next()
        
            for cell in self.current_row:
    
                cell = cell.internal_value
                
                if (column in self.first_list_columns):
                    first_list.append(cell)
                else:
                    if (column in self.second_list_columns):
                        second_list.append(cell)
                
                column += 1
                
        except StopIteration:
            return None            
            
        return Source(first_list, second_list)